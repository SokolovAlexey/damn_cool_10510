#include <stdio.h>
#include <winsock2.h>
#include <cstdlib>
 

struct header{
  int size;
  int to, from;
};

struct message{
  header h;
  char* body;
};

int main()
{
    WORD ver = MAKEWORD(2,2);
    WSADATA wsaData;
    int retVal=0;
 
    WSAStartup(ver,(LPWSADATA)&wsaData);
 
    LPHOSTENT hostEnt;
 
    hostEnt = gethostbyname("localhost");
 
    if(!hostEnt)
    {
        printf("Unable to collect gethostbyname\n");
        WSACleanup();
        return 1;
    }
 
    //������� �����
    SOCKET clientSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
 
    if(clientSock == SOCKET_ERROR)
    {
        printf("Unable to create socket\n");
        WSACleanup();
        return 1;
    }
 
    SOCKADDR_IN serverInfo;
 
    serverInfo.sin_family = PF_INET;
    serverInfo.sin_addr = *((LPIN_ADDR)*hostEnt->h_addr_list);
    serverInfo.sin_port = htons(1111);
 
    retVal=connect(clientSock,(LPSOCKADDR)&serverInfo, sizeof(serverInfo));
    if(retVal==SOCKET_ERROR)
    {
        printf("Unable to connect\n");
        WSACleanup();
        return 1;
    }
 
    printf("Connection made sucessfully\n");
 
    char *pBuf = "Request";
 
    printf("Sending request from client\n");
    retVal = send(clientSock, pBuf, strlen(pBuf)+1, 0);
    //printf("%d", strlen(pBuf));
 
    if(retVal == SOCKET_ERROR)
    {
        printf("Unable to send\n");
        WSACleanup();
        return 1;
    }
 
 
    header h;
    retVal = recv(clientSock, (char*)(&h), sizeof(header) , 0);

    char *msg = (char*)malloc(h.size);
    retVal = recv(clientSock, msg, h.size , 0);
 
    if(retVal == SOCKET_ERROR)
    {
        printf("Unable to recv\n");
        WSACleanup();
        return 1;
    }
 
    printf("Got the response from server\n%s\n",msg);
 
    closesocket(clientSock);
    WSACleanup();
	system("PAUSE");
    return 0;
}